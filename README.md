# Dokumentasi Keycloak

Keycloak adalah Identity Access Management (IAM) untuk mempermudah proses pengelolaan autentikasi dan autorisasi.

## Cara menjalankan

Beberapa persyaratan untuk menjalankan Keycloak

1. Pastikan sudah terinstall Docker dan Docker Compose

Cara menjalankan:

1. Clone repository ini
2. Ketik `docker-compose up -d --build`
3. Tunggu hingga container Keycloak sudah siap
4. Buka `<`


Video tutorial:

[![Watch the video](https://img.youtube.com/vi/WRNOCh_Cu8A/maxresdefault.jpg)](https://youtu.be/WRNOCh_Cu8A)

## Prerequisites:

We must've installed and understood:

1. Docker
2. Docker Compose
3. cURL

## Testing Keycloak endpoint

```
curl -H "Content-Type: application/x-www-form-urlencoded" -d "client_id=admin-cli" -d "username=admin" -d "password=admin" -d "grant_type=password" http://localhost:8040/realms/master/protocol/openid-connect/token
```
